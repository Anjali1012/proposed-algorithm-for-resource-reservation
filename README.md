Link of referred dataset for experiments:https://www.spec.org/power_ssj2008/results/power_ssj2008.html


Problem Statement:
Graph coloring based resource allocation in cloud data centers 

The idea is to allot the Requested Resource Unit(RRU) represented by the vertices of the graph, associated to Virtual Resource Unit(VRU) in a Virtual Machine(VM) and find an optimal mapping between the requested resources, expressed as RRU and the infrastructure servers whose capacity is measured in VRUs.
Given a graph G= (V, P) and an integer k, a proper k coloring of a graph G is an assignment of distinct k colors to each vertex (V of graph G) such that no two adjacent vertices have the same color. The number of VM nodes per VM, RRU(demand)  is uniformly distributed between 1 and 20( namely small, large, X-large, high-CPU-medium, and high-CPU-X-large have respectively 1, 4, 8, 5 and 20). The number of VMs per request(Capacity)is uniformly distributed between 11 and 15. The Revenue per request is uniformly distributed between 1 to 15. To simulate a heterogeneous data center, we refer to the SPECpower ssj2008 benchmark [14] that evaluates the performance per watt metric of servers. We define interruptions as the number of times the CPU jumped to look for availability of clusters for satisfying the demand, and assign the cost of $1 as the migration cost. 

General Algorithm Followed for all the models: 
Function: Check For Demand(Creating the list of demand):

Sort the nodes in the decreasing order of revenue/eigenvalue/demand
Check the color of the node that arrives
If color of the arriving node ∈ color used then
	Check number of demands satisfied by the color:
	If demand(arriving) > sum of demands satisfied by that color
		demand(arriving)= demand(arriving)- sum of all demands
		Store the demand associated with the color( in a dictionary)
		Store the demand in a separate list
	Check for Color Cluster
Else:
Check for Color Cluster
Store the demand associated with the color (in a dictionary)
Store the color in color used
Store the demand in a separate list

Function: Check for Color Cluster:

If demand > max(capacity) then
	Check sum(capacity) > demand(arriving)
	Select set of color clusters such that interruption is minimum
	For i in capacity do
	Demand = demand- capacity(i)
	capacity(i)=0

	i =i+1
	If capacity(i)> demand then
		capacity(i)= capacity(i)- demand
		Break
	Else:
	Continue
	Break
Else:
	Continue


Example: Consider a graph G(8, 0.6)

Output:(Graph)




Revenue Respectively: [ 9  3  9  7 14 11 10  8]
Color of the nodes:[1, 2, 2, 3, 4, 3, 5, 4]
Demand generated:[8, 20, 5, 1, 4, 1, 1, 8]
PPW values:[262, 571, 269,292, 151, 171]
Capacity:[11, 12, 15, 13, 11, 12]




MODEL 1( Sorting nodes in decreasing order of revenue):

Nodes on the basis of decreasing order of revenue [4, 5, 6, 0, 2, 7, 3, 1]
The colors required for the respective node (sorted on the basis of revenue): [4, 3, 5, 1, 2, 4, 3, 2]

Node: Respective color
{4: 4, 5: 3, 6: 5, 0: 1, 2: 2, 7: 4, 3: 3, 1: 2}

The initial node that arrives: 4
Corresponding color: 4 ⇒ color_used=[4]
Demand: 4 ⇒ demand=[4]
Dictionary of color and demand ⇒ [4:4]

Next node: 5
Corresponding color: 3 ⇒ color_used=[4, 3]
Demand: 1 ⇒ demand=[4, 1]
Dictionary of color and demand ⇒ [4:4, 3:1]

Next node: 6
Corresponding color: 5 ⇒ color_used=[4, 3, 5]
Demand: 1 ⇒ demand=[4, 1, 1]
Dictionary of color and demand ⇒ [4:4, 3:1, 5:1]

Next node: 0
Corresponding color: 1 ⇒ color_used=[4, 3, 5, 1]
Demand: 8 ⇒ demand=[4, 1, 1, 8 ]
Dictionary of color and demand ⇒ [4:4, 3:1, 5:1, 1:8]

Next node: 2
Corresponding color: 2⇒ color_used=[4, 3, 5,1, 2]
Demand: 5 ⇒ demand=[4, 1, 1, 8, 5]
Dictionary of color and demand ⇒ [4:4, 3:1, 5:1,1:8, 2:5]

Next node: 7
Corresponding color: 4 ⇒ color_used=[4, 3, 5,1,2]
Demand: 8 (8-4, as color 4 has already been allocated and node 7 needs 4 more servers to satisfy the demand) ⇒ demand=[4, 1, 1, 8, 5, 4]
Dictionary of color and demand ⇒ [(4:4, 4), 3:1, 5:1, 1:8,2:5]




Next node: 3
Corresponding color: 3 ⇒ color_used=[4, 3, 5,1,2]
Demand: 1 (1-1, as color 3 has already been alotted and node 3 needs 0 more servers to satisfy the demand) ⇒ demand=[4, 1, 1, 8, 5, 4]
Dictionary of color and demand ⇒ [(4:4, 4), 3:1, 5:1, 1:8, 2:5]

Next node: 1
Corresponding color: 2 ⇒ color_used=[4, 3, 5, 1, 2]
Demand: 20 (20-5, as color 2 has already been alotted and node 1 needs 15 more servers to satisfy the demand) ⇒ demand=[4, 1, 1, 8, 5, 4,15]
Dictionary of color and demand ⇒ [(4:4, 4), 3:1, 5:1, 1:8, (2:5, 15)]

The demands are as follows: [4, 1, 1, 8, 5, 4,15]
The capacity of the servers are as follows: [11, 12, 15, 13, 11, 12]
Demand:4 ⇒ Capacity:[7, 12, 15, 13, 11, 12]
Demand:1 ⇒ Capacity:[6, 12, 15, 13, 11, 12]
Demand:1 ⇒ Capacity:[5, 12, 15, 13, 11, 12]
Demand:8 ⇒ Capacity:[5, 4, 15, 13, 11, 12]
Demand:5 ⇒ Capacity:[0, 4, 15, 13, 11, 12]
Demand:4 ⇒ Capacity:[0, 0, 15, 13, 11, 12]
Demand:15 ⇒ Capacity:[0	, 0, 0, 13, 11, 12]

The total number of servers used = 3(1, 2, 3)
PPW for server1, 2 and 3 are corresponding:262, 571, 269
Total revenue Generated: 71
The total PPW: 1102ppw
Total number of servers used(total numbers of demands/ colors alotted): 38
Total Migration: 0
Total Time: 34milliseconds


MODEL 2 (On The Basis of Degree Centrality):
Degree of all the nodes of G(8, 0.6):
{0: 0.8571428571428571, 1: 0.2857142857142857, 2: 0.42857142857142855, 3: 0.8571428571428571, 4: 0.5714285714285714, 5: 0.5714285714285714, 6: 0.5714285714285714, 7: 1.0}
Revenue Respectively: [ 9  3  9  7 14 11 10  8]
Demand generated:[8, 20, 5, 1, 4, 1, 1, 8]

Nodes sorted on the basis of degree: [7, 0, 3, 4, 5, 6, 2, 1]
Node: Respective color
{7: 4, 0: 1, 3: 3, 4: 4, 5: 3, 6: 5, 2: 2, 1: 2}

The initial node that arrives: 7
Corresponding color: 4 ⇒ color_used=[4]
Demand: 8 ⇒ demand=[8]
Dictionary of color and demand ⇒ [4:8]

Next node: 0
Corresponding color: 1 ⇒ color_used=[4, 1]
Demand: 8 ⇒ demand=[8, 8 ]
Dictionary of color and demand ⇒ [4:8, 1:8,]

Next node: 3
Corresponding color: 3 ⇒ color_used=[4, 1,3]
Demand: 1 ⇒ demand=[8, 8 ,1]
Dictionary of color and demand ⇒ [4:8, 1:8, 3:1]

Next node: 4
Corresponding color: 4 ⇒ color_used=[4, 1,3]
Demand: 0(as 4<8, as color 4 has already been used for eight servers and node 4 needs 0 more servers to satisfy the demand)⇒ demand=[8, 8 ,1]
Dictionary of color and demand ⇒ [4:8, 1:8, 3:1]

Next node: 5
Corresponding color: 3 ⇒ color_used=[4, 1,3]
Demand:0(1-1, as color 3 has already been used for one server and node 4 needs 0 more servers to satisfy the demand) ⇒ demand=[8, 8 ,1]
Dictionary of color and demand ⇒ [4:8, 1:8, 3:1]

Next node: 6
Corresponding color: 5 ⇒ color_used=[4, 1,3,5]
Demand: 1 ⇒ demand=[8, 8 ,1,1]
Dictionary of color and demand ⇒ [4:8, 1:8, 3:1, 5:1]

Next node: 2
Corresponding color: 2 ⇒ color_used=[4, 1,3,5, 2]
Demand: 5⇒ demand=[8, 8 ,1,1, 5]
Dictionary of color and demand ⇒ [4:8, 1:8, 3:1, 5:1, 2:5]

Next node: 1
Corresponding color: 2 ⇒ color_used=[5, 1,3,2, 4]
Demand: 15(20-5, as color 2 has already been used for 5 servers and node 1 needs 15 more servers to satisfy the demand)⇒ demand=[8, 8 ,1,1,5,15]
Dictionary of color and demand ⇒ [4:8, 1:8, 3:1, 5:1, (2:5, 15)]

The demands are as follows: [8, 8, 1, 1, 5, 15]
The capacity of the servers are as follows: [11, 12, 15, 13, 11, 12]
Demand:8 ⇒ Capacity:[3, 12, 15, 13, 11, 12]
Demand:8 ⇒ Capacity:[3, 4, 15, 13, 11, 12]
Demand:1 ⇒ Capacity:[2, 4, 15, 13, 11, 12]
Demand:1 ⇒ Capacity:[1, 4, 15, 13, 11, 12]
Demand:5 ⇒ Capacity:[1, 4, 10, 13, 11, 12]
Demand:15(15>max(capacity)) ⇒ Capacity:[0, 4, 10, 13, 11, 12]
Interruption=1
	Remaining Demand:(15-1=14) ⇒ Capacity:[0, 0, 10, 13, 11, 12]
Interruption=2
	Remaining Demand:(14-4=10) ⇒ Capacity:[0, 0, 0, 13, 11, 12]


The total number of servers used = 3(1, 2, 3)
PPW for server1, 2 and 3 are corresponding:262, 571, 269
Total revenue Generated: 71
The total PPW: 1102ppw
Total number of servers used(total numbers of demands/ colors alotted): 35
Total Migration: 2
Migration cost: $1(for each migration)
Total Migration Cost: $2
Total Time: 34milliseconds

MODEL 3 and 4(On The Basis of Demand):
Revenue Respectively: [ 9  3  9  7 14 11 10  8]
Demand generated:[8, 20, 5, 1, 4, 1, 1, 8]
Demand in the decreasing order:[20, 8, 8, 5, 4, 1,1,1]
Nodes sorted in the order of decreasing order of demand:[1, 0, 7, 2, 4, 3, 5, 6]
Color of the nodes allotted(in decreasing order of demand):[2, 1, 4, 2, 4, 3, 3, 5]

The initial node that arrives: 1
Corresponding color: 2 ⇒ color_used=[2]
Demand: 20 ⇒ demand=[20]
Dictionary of color and demand ⇒ [2:20]

Next node: 0
Corresponding color: 1 ⇒ color_used=[2,1]
Demand: 8 ⇒ demand=[20, 8]
Dictionary of color and demand ⇒ [2:20, 1:8]



Next node: 7
Corresponding color: 4 ⇒ color_used=[2,1,4]
Demand: 8 ⇒ demand=[20, 8, 8]
Dictionary of color and demand ⇒ [2:20, 1:8, 4:8]

Next node: 2
Corresponding color: 2 ⇒ color_used=[2,1,4]
Demand: 0(5<8, as color 2 has already been used for 8 servers and node 2 needs 0 more servers to satisfy the demand)  ⇒ demand=[20, 8, 8]
Dictionary of color and demand ⇒ [2:20, 1:8, 4:8]

Next node: 4
Corresponding color: 4 ⇒ color_used=[2,1,4]
Demand: 0(4<8, as color 4 has already been used for 8 servers and node 4 needs 0 more servers to satisfy the demand)  ⇒ demand=[20, 8, 8]
Dictionary of color and demand ⇒ [2:20, 1:8, 4:8]

Next node: 3
Corresponding color: 3 ⇒ color_used=[2,1,4, 3]
Demand: 1  ⇒ demand=[20, 8, 8, 1]
Dictionary of color and demand ⇒ [2:20, 1:8, 4:8, 3:1]

A similar process will take place for node 5 and 6. The final demand list will be [20, 8, 8, 1, 1]
The capacity of the servers are as follows: [11, 12, 15, 13, 11, 12]
Demand:20 (20> max(capacity)) ⇒ Capacity:[0, 12, 15, 13, 11, 12]
(Interruption=1)
Remaining Demand 9: ⇒ Capacity:[0, 3, 15, 13, 11, 12]
Demand:8 ⇒ Capacity:[0, 3, 7, 13, 11, 12]
Demand:8 ⇒ Capacity:[0, 3, 7, 5, 11, 12]
Demand:1 ⇒ Capacity:[0, 2, 7, 5, 11, 12]
Demand:1 ⇒ Capacity:[0, 1, 7, 5, 11, 12]

The total number of servers used = 4(1, 2, 3, 4)
PPW for server1, 2 , 3 and 4 are corresponding:262, 571, 269,292
Total revenue Generated: 71
The total PPW: 1394ppw
Total number of servers used(total numbers of demands/ colors alotted): 38
Total Migration: 1
Migration Cost: $1
Total Time: 34milliseconds

In the case of MODEL 4:( Getting demand from the max color capacity):
It follows a little different allotment of colors from the clusters: 
Demand arrived:[20, 8, 8, 1, 1]
The capacity of the servers are as follows: [11, 12, 15, 13, 11, 12]
Demand:20 (20> max(capacity)) ⇒ Capacity:[0, 12, 15, 13, 11, 12]
(Interruption=1)
Remaining Demand 9: (9<max(capacity), hence max(capacity)-demand remaining)
⇒ Capacity:[0, 12, 6, 13, 11, 12]
Demand:8 ⇒ Capacity:[0, 4, 6, 13, 11, 12]
Demand:8 ⇒ Capacity:[0, 4, 6, 5, 11, 12]
Demand:1 ⇒ Capacity:[0, 3, 6, 5, 11, 12]
Demand:1 ⇒ Capacity:[0, 2, 6, 5, 11, 12]

The total number of servers used = 4(1, 2, 3, 4)
PPW for server1, 2 , 3 and 4 are corresponding:262, 571, 269,292
Total revenue Generated: 71
The total PPW: 1394ppw
Total number of servers used(total numbers of demands/ colors alotted): 38
Total Migration: 1
Migration Cost: $1
Total Time: 34milliseconds

Used to reduce the number of interruptions, but leads to increases in the number of servers used for a large number of nodes. 



















	


























