# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 13:11:58 2020

@author: HP
"""

#IMPORTING LIBRARIES

import numpy as np
import time
import operator
import random
from collections import defaultdict
try:
    import matplotlib.pyplot as plt
except:
    raise
import networkx as nx    
watt=[]   
servers_used=[]
interruptions=[]
total_colors=[]
rev=[]

#fig= plt.figure(figsize=(5,5))
start = time.time()

for u in range(0, 1):
    probability=1
    no_of_vertices=4
    #----------
    
    #PLOTTING GRAPH
    G = nx.erdos_renyi_graph(no_of_vertices, probability)
    #nx.cycle_graph(G)
    pos=nx.spring_layout(G,iterations=200)
    #nx.draw(G,pos,with_labels=True, node_color=range(no_of_vertices),node_size=800,cmap=plt.cm.spring)
    #plt.savefig("node_colormap.png") # save as png
    #plt.show()
    
    #-----------
    #DEFINING CONFIGURATION:
    
    #GENERATING DEMAND
    random_no=[]
    for i in range(0, no_of_vertices):
    
        w=random.choice([1, 4, 8, 5, 20])
        random_no.append(w)
#    print("Demand",random_no)
    demand=random_no[0:len(random_no)]
    
    #CPACITIES OF THE SERVER
    size_lst=300
    capacity=np.random.randint(11, 16, size=size_lst)
    #print("\nCapacity", capacity)
    
    len_lst=[]
    for i in range(0, size_lst):
        len_lst.append(capacity[i])
    #print("\nLen lst", len_lst)
    
    
    #COPY OF LIST OF CAPACITY
    updated_length=len_lst[0:len(len_lst)+1]
    
    #GENERATING REVENUE
    revenue=np.random.randint(1, 16, size=no_of_vertices)
    #print("\nRevenue Generated", revenue)
    rev.append(sum(revenue))
    #MIGRATION COST
    migration_cost=1
    #print("\nMigration cost", migration_cost)
    
    
    #PERFORMANCE PER WATT
    ppw=[]
    for i in range(0, size_lst):
    
        t=random.choice([385, 1417, 173, 170, 118, 315, 269, 206,195,254,\
                         196, 386, 784, 197, 198, 193, 192, 95.4,260, 259,\
                         596, 597, 272, 259, 257,245, 244, 267, 70.3,1111,\
                         273, 518, 124, 125, 127, 265, 1014, 1016, 524,571,\
                         295, 262, 102])
        ppw.append(t)
    #print("\n PPW values of respective cluster", ppw)
    
    
    #----------------
    
    
    color_list=[]
    
        
    
    V = no_of_vertices 
    graph = [[0 for column in range(no_of_vertices)]\
                      for row in range(no_of_vertices)] 
      
    # A utility function to check if the current color assignment 
    # is safe for vertex v 
    def isSafe(v, colour, c): 
        for i in range(V): 
            if graph[v][i] == 1 and colour[i] == c: 
                return False
        return True
      
    # A recursive utility function to solve m 
    # coloring  problem 
    def graphColourUtil(m, colour, v): 
        if v ==V: 
            return True
      
        for c in range(1, m+1): 
            if isSafe(v, colour, c) == True: 
                colour[v] = c 
                if graphColourUtil(m, colour, v+1) == True: 
                    return True
                colour[v] = 0
      
    def graphColouring(m): 
        colour = [0] * V 
        if graphColourUtil(m, colour, 0) == None: 
            return False
      
        # Print the solution 
        color_list=[]
        cl=[]
        #print("Solution exist and Following are the assigned colours:")
        for c in colour: 
            #print(c)
            color_list.append(c)
    #        cl=[]
        for q in nodes_with_decdem:
            cl.append(color_list[q])
    #        print("Color List", color_list)
            
    #        print("\nThe colors required for respective node\n" + str(cl))
    
    #-------------------        
        #GENERATING SEQUENCE OF DEMANED COLORS TO SEND
        
        res = defaultdict(list)
        for i, j in zip(cl, random_no ):
            res[i].append(j)
            
    #        print(res)
        order= res.keys()
    #        print(order)
        
        demanded_colors=[]
        #demand_sum=0
        for i in order:
            demanded_colors.append(max(res[i]))
            #demand_sum+= max(res[i])
    #        print("\nThe colors needed in a list ", demanded_colors)
        #print("\nSum of all the max values" + str(demand_sum))
        demanded_colors= sorted(demanded_colors, reverse= True)
    #        print("Demanded colors", demanded_colors)
        total_colors.append(sum(demanded_colors))     
        #DEFINING VARIABLES FOR CODE
        j=0
        count=0
       
        inter=0
        r=0
        
      
      
        #CODE FOR ALLOTING CLUSTERS
        for i in demanded_colors:
            for k in range(0, len(len_lst)):
                sum_len=0
                if i> max(len_lst):
                    for n in len_lst:
                        sum_len= sum_len+n
                
            
                
                    if sum_len>=i:
                        #print(sum_len,"something can be done")
                        inter=inter+1
    #                        print("Inter", inter)
                        
                        for e in range(0, len(len_lst)):
                            r= r+len_lst[e]
                            if r>=i:
                                #print(r, i,e,  "possible")
                                for m in range(0, len(len_lst)):
    #                                    print(i)
                                    i= i-len_lst[m]
                                    len_lst[m]=0  
    #                                    print("\n",len_lst)
    #                                    print(i)
    #                                    print("IIIIIIII", inter)
                             
                                    if i<=max(len_lst):
                                        w=len_lst.index(max(len_lst))
                                        len_lst[w]=len_lst[w]-i
    #                                        print(len_lst)
                                        #inter=inter+1
                                        #print("\ninter", inter)
                                        
                                        break
                                break
                        break
                    break        
    #               
                        
                if i<= len_lst[k]:
                    count=count+1    
                    len_lst[k]= len_lst[k]-i
    #                    print("\n", i)
    #                    print(len_lst)                
                    break
                        
                        
                if i > max(len_lst) and sum_len<i:
    #                    print("\nNothing can be done")
                    break
                
                #                
                else:
                    continue
       
                
        index_of_servers_used=[]
        for h in range(0, len(len_lst)):
            for j in range(0, len(updated_length)):
                if updated_length[h]!=len_lst[h]:
                    index_of_servers_used.append(h)
                    break
                else:
                    continue
                
        #TOTAL PERFORMANCE PER WATT   
        sum_of_ppw=0       
    #        print("\nIndices of he servers used", index_of_servers_used)
        for i in index_of_servers_used:
            sum_of_ppw+=ppw[i]
        watt.append(sum_of_ppw)
#        print("\nThe ppw for", no_of_vertices, "nodes with pribability",probability,"is", sum_of_ppw)
        
        #TOTAL CLUSTERS
#        print("Total clusters used are:" , len(index_of_servers_used))
        servers_used.append(len(index_of_servers_used))
        
        
        #TOTAL INTERRUPTIONS:
#        print("The total number of interruptions are ", (inter))
        interruptions.append(inter)
        #MIGRATION COST: 
#        print("The cost for {} interruptions is {}".format(inter,\
#              (migration_cost)*(inter)))
#-----------------------------------------------       
        #SOME STATEMENTS(uncomment to know more):
#        print(demand)
#        print("\nCapacity", capacity)
#        print("\nColor: Demand of nodes using that color\n", res)
#        print("\nIndices of he servers used", index_of_servers_used)
#        print("\nThe ppw for", no_of_vertices, "nodes is", sum_of_ppw)
#        print("The total number of interruptions are ", (inter))
#        
#------------------------------------------------------        
#              
        return True
            
  #FUNCTION TO GET THE ADJACENCY MATRIX  
     
    def gen_edges(grap):
        edge = []
        for node in grap:
            x = []
            for n in range(len(grap)):
                if n in grap[node]:
                    x.append(1)
                else:
                    x.append(0)
            edge.append((node, x))
            
        return edge
    
    
    
    #------------------------------------------------------------------
    
    f=gen_edges(G)
    #print(f)
    m=len(f)
    
    no_of_nodes=[]
    for i in range(m):
        #print(i)
        no_of_nodes.append(i)
    #print("\nNo of nodes"+ str(no_of_nodes))
    #print("\nDemand generated randomly", random_no)
    random_new= sorted(random_no, reverse= True)
    #print("Random_New", random_new)
    
    
    
    dictionary = dict(zip(no_of_nodes, random_no))
    #print("\nNode: Demand\n" + str(dictionary))

    
    nodes_with_decdem=sorted(dictionary, key= dictionary.get, reverse= True)
 
    arr= []
    for i in range(len(f)):
        arr.append(f[i][1])
    #print(arr)

    graph = arr

    
    graphColouring(m)

end = time.time()
print("On the basis of Demand(Getting colors from the clusters with max length first)")
print("number of vertices =", no_of_vertices, " and probability=", probability)
#print(rev)
print("Revenue Generated", (sum(rev))/(len(rev)))
#print(watt)
print("The ppw value:", (sum(watt))/(len(watt)))
#print(total_colors)

try:
    print("Total colors", (sum(total_colors))/(len(total_colors)))
except ZeroDivisionError:
    print("Zero")


#print(servers_used)
print("The number of servers used", (sum(servers_used))/(len(servers_used)))
#print(interruptions)
print("The interruptions", (sum(interruptions))/(len(interruptions)))
                        
print("The total Time required is",(end-start)/len(watt))

